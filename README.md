# Repositorio de Datos

  Este repositorio tiene un informe *automático* que se puede configurar para generar gráficos y tablas en base a una fuente sencilla de datos.
  Gitlab comparte los informes generados en línea usando *GitLab pages*.
  Se puede crear varios informes en base a una o varias fuentes de datos. Abajo dejamos ejemplos.

## NOTA IMPORTANTE

Este es el ejemplo para el proyecto. La idea es que puedas incorporar esta herramienta en un repositorio propio y obtener tu propio informe automático. 


* La salida para este repo está en: https://octavioduarte.gitlab.io/auto_documentador_ci/informe.html


# Cómo configurarlo para tu uso

  En este caso, todos los procesos pasan adentro de un archivo que se llama `./documentador_r/informe_esqueleto.Rmd`. Este es un archivo de tipo Rmarkdown, que combina código en R con texto Markdown y ejecuta el código en R para enriquecerse e interactuar con la red. 
  Este muestra tablas y gráficos y se configura al llamarlo a compilación adentro del archivo `.gitlab-ci.yml`.
  El que proveemos acá es *flexible* y por lo tanto se puede usar sin aprender a escribir código en R.
  Con el tiempo, podés reemplazarlo por un archivo en otro formato que compile a HTML o editar el código en R disponible acá. 
  Si necesitás crear una versión personal para tu projecto, por favor contactá al autor de este repositorio. Voy a hacer todo lo posible para ayudarte.
  
  
## Ejemplo de cómo configurar un informe.

### Elementos Involucrados

  Vamos a repetir literalmente estos item en comentarios arriba de la llamada a la función para hacer bien obvio dónde van.
  Después, comentamos con un poco de detalle qué es cada uno.

1. Proveer un *archivo fuente de datos* en un formato adecuado.
1. Decidir qué *tipo de gráfico* queremos usar.
1. Decidir qué *variables queremos mostrar* en el gráfico.
1. Nombre del archivo destino.

### Llamada Ejemplo

```r
rmarkdown::render(
 ## esto NO se cambia, en general usamos este informe que se modifica respondiendo a los parámetros
 input="./documentador_r/informe_esqueleto.Rmd",
 ## archivo destino
 output_file = "../public/histograma_temperatura.html",
 params=list(
   ## variables que queremos mostrar
   variable_x = "temperatura",
   ## tipo de gráfico
   grafico="histograma",
   ## archivo fuente de datos
   archivo_csv="datos_ejemplo.csv" 
) 
)
```

### Detalles sobre cada elemento
